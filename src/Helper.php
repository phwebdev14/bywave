<?php

namespace Bywave;

/**
 * Just contains helpful functions
 */
class Helper {
	/**
	 * [log prints simple string/array]
	 * @param  string $data [data to print]
	 * @return [type]       [void]
	 */
	function log( $data = "" ){

		return is_array( $data ) ? print_r( $data ) : print( $data . PHP_EOL );		
		
	}

	/**
	 * [distinct_array generates array with distict values]
	 * @param  integer $min [minimum distinct range]
	 * @param  integer $max [maximum distinct range]
	 * @param  integer $n   [size of output]
	 * @return [array]       [distinct array]
	 */
	function distinct_array( $min = 0, $max = 0, $n = 0 ){
		
		$numbers = range($min, $max);
	    
	    shuffle($numbers);
	    
	    return array_slice($numbers, 0, $n);

	}

	/**
	 * [analog_array generates array with random values either 0 and 1]
	 * @param  integer $n [size of output]
	 * @return [type]     [analog array]
	 */
	function analog_array( $n = 0 ){
		
		$array = [];
		
		while ( count( $array ) < $n + 1 ) {
			
			$array[] = rand(0, 1);
		
		}
		
		return $array;

	}

}
<?php

namespace Bywave;
/**
 * Contains solution for question #1
 */
class Fish {
	
	public $survived = null;

	/**
	 * [solution calculates the number of fish that will survived with a given random direction]
	 * @param  array  $A [fishes with value]
	 * @param  array  $B [$A directions]
	 * @return [type]    [number of fishes survives]
	 */
	function solution( $A = array(), $B = array() ){

		// Basic rule:
		// Fish with the highest value shall survive
		
		$survived = 0;
		
		$downstream = [];

		for( $i = 0; $i < count($A); $i++ ){

			
			if( $B[$i] === 1 ){
				
				$downstream[] = $A[$i];
			
			}else{

				do {
					
					if( $downstream[ count( $downstream ) - 1 ] < $A[$i] ){
						// $A[$i] eats the downstream fish
						array_pop($downstream);

					}else{

						break;

					}

				} while ( count( $downstream ) != 0 );

				$survived += !count($downstream) ? 1 : 0;

			
			}

		}

		$survived += count($downstream);

		return $this->survived = $survived;

	}


}
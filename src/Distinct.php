<?php

namespace Bywave;
/**
 * Contains solution for counting number of distinct values in an array
 */
class Distinct {

	public $count = null;
	
	/**
	 * [solution counts the numebr of distict values in an array]
	 * @param  array  $A [subject]
	 * @return [type]    [number of distinct values]
	 */
	function solution( $A = array(  ) ){

		// Sort array ascendingly
		sort($A);
		
		// Any value that's cannot be within $A
		$last_unique = 'x';
		
		// Number of distinct counter
		$distinct = 0;

		// Main process
		foreach ($A as $a) {
			
			if( $a !== $last_unique ){
				
				$last_unique = $a;

				$distinct++;
			
			}
		}

		return $this->count = $distinct;

	}

}
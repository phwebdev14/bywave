<?php

require __DIR__ . '/vendor/autoload.php';

use Bywave\Helper;

use Bywave\Fish;

use Bywave\Distinct;

// Contains helper functions that generates 
$helper = new Helper;

// For problem #1
$fish = new Fish;

// Using testcase from the question
$helper->log( 'Testing from question #1 example' );

$fish->solution( array( 4,3,2,1,5 ), array(0,1,0,0,0) );

$helper->log( 'Number of fish survived: ' . $fish->survived );

// // Test case from 10k - 100k : 10k increment
// // A is an integer within the range [0..1,000,000,000]
// for( $i = 10000; $i <= 100000; $i+= 10000 ){

// 	$helper->log( '------' );

// 	$helper->log( 'Testing for ' . $i . ' fishes' );
	
// 	$helper->log( 'Creating distict fishes between 0 and ' . $i );

// 	$fish->solution( $helper->distinct_array( 0, $i, $i ), $helper->analog_array( $i ) );
	
// 	$helper->log( 'Number of fish survived: ' . $fish->survived );

// }


$helper->log( '---------------------------------------------------------------' );



// For problem #2
$distict = new Distinct;

// Using testcase from the question
$helper->log( 'Testing from question #2 example' );

$distict->solution( array( 2,1,1,2,3,1 ) );

$helper->log( 'Number of distinct value: ' . $distict->count );

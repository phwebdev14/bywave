# Description #
Byware Philippines Inc. Senior Web Developer Test

**Developer/Examinee:** Ian G. de la Cruz

**Date:** Feb. 19, 1017

**Time started:** 11:00 PM

**Time ended:** 12:41 PM

## Questionnaire ##
Each item is good for one hour

###1.) Fish###
**N Voracious fish** are moving along a river. Calculate how many fish are alive.
You are given two non-empty zero-indexed arrays A and B consisting of N integers. Arrays A and B represent N voracious fish in a river, ordered downstream along the flow of the river. The fish are numbered from 0 to N − 1. If P and Q are two fish and P < Q, then fish P is initially upstream of fish Q. Initially, each fish has a unique position.
Fish number P is represented by A[P] and B[P]. Array A contains the sizes of the fish. All its elements are unique. Array B contains the directions of the fish. It contains only 0s and/or 1s, where:
* 0 represents a fish flowing upstream,
* 1 represents a fish flowing downstream.

If **two fish move in opposite directions** and there are no other (living) fish between them, they will eventually meet each other. Then only one fish can stay alive − **the larger fish eats the smaller one**. More precisely, we say that two fish P and Q meet each other when P < Q, B[P] = 1 and B[Q] = 0, and there are no living fish between them. After they meet:

* If A[P] > A[Q] then P eats Q, and P will still be flowing downstream,
* f A[Q] > A[P] then Q eats P, and Q will still be flowing upstream.

We assume that all the fish are flowing at the same speed. That is, fish moving in the same direction never meet. The goal is to calculate the number of fish that will stay alive.
For example, consider arrays A and B such that:

 A[0] = 4 A[1] = 3 A[2] = 2 A[3] = 1 A[4] = 5
 B[0] = 0 B[1] = 1 B[2] = 0 B[3] = 0 B[4] = 0

Initially all the fish are alive and all except fish number 1 are moving upstream. Fish number 1 meets fish number 2 and eats it, then it meets fish number 3 and eats it too. Finally, it meets fish number 4 and is eaten by it. The remaining two fish, number 0 and 4, never meet and therefore stay alive.
Write a function:

*function solution($A, $B);*

that, given two non-empty zero-indexed arrays A and B consisting of N integers, returns the number of fish that will stay alive.
For example, given the arrays shown above, the function should return 2, as explained above.
Assume that:

* N is an integer within the range [1..100,000];
* each element of array A is an integer within the range [0..1,000,000,000];
* each element of array B is an integer that can have one of the following
values: 0, 1;
* the elements of A are all distinct.

Complexity:

* expected worst-case time complexity is O(N);
** expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments). Elements of input arrays can be modified.



###2) Distinct###
Compute number of distinct values of array

Write a function
*function solution($A);*

that, given a zero-indexed array A consisting of N integers, *returns the number of distinct values in array A*.
Assume that:

* N is an integer within the range [0..100,000];
* each element of array A is an integer within the range [−1,000,000..1,000,000].

For example, given array A consisting of six elements such that: A[0] = 2 A[1] = 1 A[2] = 1
A[3] = 2 A[4] = 3 A[5] = 1

the function should return 3, because there are 3 distinct values appearing in array A, namely 1, 2 and 3.
Complexity:

* expected worst-case time complexity is O(N*log(N));
* expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).

Elements of input arrays can be modified.

##Solutions/Idea##

###1.) Fish###

*/src/Fish.php*

* Let the fishes flow
* Fish with the highest value shall survive

###1.) Distinct###

*/src/Distinct.php*

* Sort the array ascendingly
* Compare each item to unrelevant value/last item matched if any


##Testing##
* Create composer autoload using *composer dump-autoload*
* Run index.php to see results

